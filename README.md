# README #

This README would normally document whatever steps are necessary to get your application up and running.

- **file**    goldfish_qemu.c
- **author**  Jongho Yang
- **date**    7/5/2017
- **version** 1.0
- **brief**	 goldfish sensor HAL

### DESCRIPTION ###
 
   This is a platform HAL implementation to communicate with
   the goldfish driver. The driver and this HAL supports three sensors,
   such as accelerometer, compass and gyroscopes.
   The compiled output will be placed out/target/product/generic/obj/lib/sensors.goldfish.so .
   The lib should be located at /system/lib/hw in the android file-system.
   To test this HAL implementation, the below points are required.
 
### Tested platform ###
   Lollipop android-5.1.1_r38
 
### File permission error problem ###
   When sensor HAL opens the driver, the system reports file permission error (13).
   To solve this problem, the below command should be executed before running.
        - chmod 777 /dev/sensor
        - echo 0 > /sys/fs/selinux/enforce
 
### Changed codes in the goldfish sensor driver ###
       1. function : goldfish_sensor_read
           changed parts : the below codes are removed for blocked read operation.
 
           if(*pos >= READ_BUFFER_SIZE)
           {
               put_user(-1, buf);	// return EOF
               printk("[Sensor Driver] %s, Already read all data !\n", __FUNCTION__);
               return 0;
           }
 
       2. function : goldfish_sensor_write
           The previous driver implementation was for the testing with "echo" command.
 
           before : data->enables = user_data - '0';
           after : data->enables = user_data;
 
### Used Android App for testing ###
    Downloaded the below apk and installed it onto a emulator by adb commend.
 	The apps displays same values when I changed the values of virtual sensor.
       - Sensor Box for Android, Sensors Multitool
 
### Test Result ###
    The values taken by the framework from the sensor_device_pick_pending_event_locked function,
    are same with the values on the "resulting values << Virtual sensors << Extended control" of
    the android emulator.
    So, it is confirmed that this HAL implementation works properly.