/*
 * Copyright (C) 2009 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/* this implements a sensors hardware library for the Android emulator.
 * the following code should be built as a shared library that will be
 * placed into /system/lib/hw/sensors.goldfish.so
 *
 * it will be loaded by the code in hardware/libhardware/hardware.c
 * which is itself called from com_android_server_SensorService.cpp
 */
 /**
 *  @file    goldfish_qemu.c
 *  @author  Jongho Yang (17078054)
 *  @date    7/5/2017
 *  @version 1.0
 *
 *  @brief	 goldfish sensor HAL
 *
 *  @section DESCRIPTION
 *
 *  This is a platform HAL implementation to communicate with
 *  the goldfish driver. The driver and this HAL supports three sensors,
 *  such as accelerometer, compass and gyroscopes.
 *  The compiled output will be placed out/target/product/generic/obj/lib/sensors.goldfish.so .
 *  The lib should be located at /system/lib/hw in the android file-system.
 *  To test this HAL implementation, the below points are required.
 *
 * - Tested platform
 *      Lollipop android-5.1.1_r38
 *
 * - File permission error problem
 *      When sensor HAL opens the driver, the system reports file permission error (13).
 *      To solve this problem, the below command should be executed before running.
 *          - chmod 777 /dev/sensor
 *          - echo 0 > /sys/fs/selinux/enforce
 *
 * - Changed codes in the goldfish sensor driver
 *      1. function : goldfish_sensor_read
 *          changed parts : the below codes are removed for blocked read operation.
 *
 *          if(*pos >= READ_BUFFER_SIZE)
 *          {
 *              put_user(-1, buf);	// return EOF
 *              printk("[Sensor Driver] %s, Already read all data !\n", __FUNCTION__);
 *              return 0;
 *          }
 *
 *      2. function : goldfish_sensor_write
 *          The previous driver implementation was for the testing with "echo" command.
 *
 *          before : data->enables = user_data - '0';
 *          after : data->enables = user_data;
 *
 * - Used Android App
 *      Downloaded the below apk and installed it onto a emulator by adb commend.
 *	The apps displays same values when I changed the values of virtual sensor.
 *      - Sensor Box for Android, Sensors Multitool
 *
 * - Test Result
 *   The values taken by the framework from the sensor_device_pick_pending_event_locked function,
 *   are same with the values on the "resulting values << Virtual sensors << Extended control" of
 *   the android emulator.
 *   So, it is confirmed that this HAL implementation works properly.
 *
 */
#define  SENSORS_SERVICE_NAME "sensors"

#define LOG_TAG "QemuSensors"

#include <unistd.h>
#include <fcntl.h>
#include <errno.h>
#include <string.h>
#include <cutils/log.h>
#include <cutils/sockets.h>
#include <hardware/sensors.h>

#if 1
#define  D(...)  ALOGD(__VA_ARGS__)
#else
#define  D(...)  ((void)0)
#endif

#define  E(...)  ALOGE(__VA_ARGS__)

#include <hardware/qemud.h>

/**
 * @brief	 SENSOR IDS AND NAMES
 */
#define MAX_NUM_SENSORS 8

#define SUPPORTED_SENSORS  ((1<<MAX_NUM_SENSORS)-1)

#define  ID_BASE           SENSORS_HANDLE_BASE
#define  ID_ACCELERATION   (ID_BASE+0)
#define  ID_MAGNETIC_FIELD (ID_BASE+1)
#define  ID_ORIENTATION    (ID_BASE+2)
#define  ID_TEMPERATURE    (ID_BASE+3)
#define  ID_PROXIMITY      (ID_BASE+4)
#define  ID_LIGHT          (ID_BASE+5)
#define  ID_PRESSURE       (ID_BASE+6)
#define  ID_HUMIDITY       (ID_BASE+7)

#define  SENSORS_ACCELERATION    (1 << ID_ACCELERATION)
#define  SENSORS_MAGNETIC_FIELD  (1 << ID_MAGNETIC_FIELD)
#define  SENSORS_ORIENTATION     (1 << ID_ORIENTATION)
#define  SENSORS_TEMPERATURE     (1 << ID_TEMPERATURE)
#define  SENSORS_PROXIMITY       (1 << ID_PROXIMITY)
#define  SENSORS_LIGHT           (1 << ID_LIGHT)
#define  SENSORS_PRESSURE        (1 << ID_PRESSURE)
#define  SENSORS_HUMIDITY        (1 << ID_HUMIDITY)

#define  ID_CHECK(x)  ((unsigned)((x) - ID_BASE) < MAX_NUM_SENSORS)

#define  SENSORS_LIST  \
    SENSOR_(ACCELERATION,"acceleration") \
    SENSOR_(MAGNETIC_FIELD,"magnetic-field") \
    SENSOR_(ORIENTATION,"orientation") \
    SENSOR_(TEMPERATURE,"temperature") \
    SENSOR_(PROXIMITY,"proximity") \
    SENSOR_(LIGHT, "light") \
    SENSOR_(PRESSURE, "pressure") \
    SENSOR_(HUMIDITY, "humidity")

/**
 * @brief	Sensor data structure .
 *          This is the data structure defined in the sensor kernel driver.
 *          Since it is not allowed to use a header file, same structure is also defined here.
 */
struct sensor_values {
	int32_t x;
	int32_t y;
	int32_t z;
};

struct sensors {
	uint32_t					which;	/// indicate that which sensor caused an interrupt
	struct sensor_values		accel;
	struct sensor_values		compass;
	struct sensor_values		gyro;
};

/**
 * @brief	bit mask to enable or disable interrupt from specified sensor
 */
enum {
	SENSOR_INT_ACCEL	= 1U << 0,
	SENSOR_INT_COMPASS	= 1U << 1,
	SENSOR_INT_GYRO 	= 1U << 2,

	SENSOR_INT_MASK     = SENSOR_INT_ACCEL | SENSOR_INT_COMPASS | SENSOR_INT_GYRO
};

#define READ_BUFFER_SIZE        (sizeof(struct sensors))

static const struct {
    const char*  name;
    int          id; } _sensorIds[MAX_NUM_SENSORS] =
{
#define SENSOR_(x,y)  { y, ID_##x },
    SENSORS_LIST
#undef  SENSOR_
};

static const char*
_sensorIdToName( int  id )
{
    int  nn;
    for (nn = 0; nn < MAX_NUM_SENSORS; nn++)
        if (id == _sensorIds[nn].id)
            return _sensorIds[nn].name;
    return "<UNKNOWN>";
}

static int
_sensorIdFromName( const char*  name )
{
    int  nn;

    if (name == NULL)
        return -1;

    for (nn = 0; nn < MAX_NUM_SENSORS; nn++)
        if (!strcmp(name, _sensorIds[nn].name))
            return _sensorIds[nn].id;

    return -1;
}

/** return the current time in nanoseconds */
static int64_t now_ns(void) {
    struct timespec  ts;
    clock_gettime(CLOCK_MONOTONIC, &ts);
    return (int64_t)ts.tv_sec * 1000000000 + ts.tv_nsec;
}

/**
 * @brief   SENSORS POLL DEVICE
 *          This one is used to read sensor data from the hardware.
 */
typedef struct SensorDevice {
    struct sensors_poll_device_1  device;
    sensors_event_t               sensors[MAX_NUM_SENSORS];
    uint32_t                      pendingSensors;
    int64_t                       timeStart;
    int64_t                       timeOffset;
    uint32_t                      active_sensors;
    int                           fd;
    pthread_mutex_t               lock;
} SensorDevice;

/**
 * @brief   Grab the file descriptor to the emulator's sensors service pipe.
 *
 *          This function returns a file descriptor on success, or -errno on
 *          failure, and assumes the SensorDevice instance's lock is held.
 */
static int sensor_device_get_fd_locked(SensorDevice* dev) {
    /** Create connection to service on first call */
    if (dev->fd < 0) {
        dev->fd = open("/dev/sensor", O_RDWR);
        int ret = -errno;
        if (dev->fd < 0) {
            E("%s: Could not open sensor driver : %s (%d) ", __FUNCTION__,
                strerror(-ret), -ret);
            return ret;
        }
        D("%s: Success to open sensor driver : %s (%d) ", __FUNCTION__,
            strerror(-ret), -ret);

    }
    return dev->fd;
}

/**
 * @brief	Send a command to the sensors virtual device.
 *          Currently, this code is not necessary.
 *          |dev| is a device instance and
 *          |cmd| is a zero-terminated command string. Return 0 on success, or -errno
 *          on failure.
 */
static int sensor_device_send_command_locked(SensorDevice* dev,
                                             const char* cmd) {
    E("%s This function should not be called", __FUNCTION__);

    int fd = sensor_device_get_fd_locked(dev);
    if (fd < 0) {
        return fd;
    }

    int ret = 0;
    if (qemud_channel_send(fd, cmd, strlen(cmd)) < 0) {
        ret = -errno;
        E("%s(fd=%d): ERROR: %s", __FUNCTION__, fd, strerror(errno));
    }
    return ret;
}

/**
 * @brief	Pick up one pending sensor event.
 *
 *          On success, this returns the sensor
 *          id, and sets |*event| accordingly. On failure, i.e. if there are no
 *          pending events, return -EINVAL.
 */
static int sensor_device_pick_pending_event_locked(SensorDevice* d,
                                                   sensors_event_t* event)
{
    uint32_t mask = SUPPORTED_SENSORS & d->pendingSensors;
    if (mask) {
        uint32_t i = 31 - __builtin_clz(mask);
        d->pendingSensors &= ~(1U << i);
        *event = d->sensors[i];
        event->sensor = i;
        event->version = sizeof(*event);

        D("%s: %d [%f, %f, %f]", __FUNCTION__,
                i,
                event->data[0],
                event->data[1],
                event->data[2]);
        return i;
    }
    E("No sensor to return!!! pendingSensors=0x%08x", d->pendingSensors);
    // we may end-up in a busy loop, slow things down, just in case.
    usleep(100000);
    return -EINVAL;
}

/**
 * @brief	Read sensor data from the goldfish driver
 *          Block until new sensor events are reported by the emulator.
 *          On success, updates the |pendingEvents| and |sensors| fields of |dev|.
 *
 * @retval 	On success, return 0. On failure, return -errno.
 */
static int sensor_device_poll_event_locked(SensorDevice* dev)
{
    D("%s: dev=%p", __FUNCTION__, dev);

    int fd = sensor_device_get_fd_locked(dev);
    if (fd < 0) {
        E("%s: Could not get pipe channel: %s", __FUNCTION__, strerror(-fd));
        return fd;
    }

    /// Accumulate pending events into |events| and |new_sensors| mask
    /// code a bit.
    uint32_t new_sensors = 0U;
    sensors_event_t* events = dev->sensors;

    int64_t event_time = -1;
    int ret = 0;

    /** Release the lock since we're going to block on recv() */
    pthread_mutex_unlock(&dev->lock);

    struct sensors sensorData;

    /** read sensor values from the sensor device. */
    int len = read(fd, (void*)&sensorData, READ_BUFFER_SIZE);

    /** re-acquire the lock to modify the device state. */
    pthread_mutex_lock(&dev->lock);

    if (len < 0) {
        ret = -errno;
        E("%s(fd=%d): Could not receive event data len=%d, errno=%d: %s",
          __FUNCTION__, fd, len, errno, strerror(errno));
        return ret;
    }

    D("%s(fd=%d): success to read ", __FUNCTION__, fd);
    D("%s(fd=%d): Which sensors ? : 0x%x \n", __FUNCTION__, fd, sensorData.which);

    /** All sensor values are 32 bit fixed point with an 8 bit fraction
        So, divide with 256.0f to get a float value. */
    ///if (sensorData.which & SENSOR_INT_ACCEL)
    {
        D("%s(fd=%d): read ACCELERATION ", __FUNCTION__, fd);
        D("%s SENSOR_ACCEL_X : 0x%x \n", __FUNCTION__, sensorData.accel.x);
        D("%s SENSOR_ACCEL_Y : 0x%x \n", __FUNCTION__, sensorData.accel.y);
        D("%s SENSOR_ACCEL_Z : 0x%x \n", __FUNCTION__, sensorData.accel.z);

        new_sensors |= SENSORS_ACCELERATION;
        events[ID_ACCELERATION].acceleration.x = ((float)sensorData.accel.x)/256.0f;
        events[ID_ACCELERATION].acceleration.y = ((float)sensorData.accel.y)/256.0f;
        events[ID_ACCELERATION].acceleration.z = ((float)sensorData.accel.z)/256.0f;
        events[ID_ACCELERATION].type = SENSOR_TYPE_ACCELEROMETER;
    }

    ///if (sensorData.which & SENSOR_INT_GYRO)
    {
        D("%s(fd=%d): read ORIENTATION ", __FUNCTION__, fd);
        D("%s SENSOR_GYRO_X : 0x%x \n", __FUNCTION__, sensorData.gyro.x);
        D("%s SENSOR_GYRO_Y : 0x%x \n", __FUNCTION__, sensorData.gyro.y);
        D("%s SENSOR_GYRO_Z : 0x%x \n", __FUNCTION__, sensorData.gyro.z);

        new_sensors |= SENSORS_ORIENTATION;
        events[ID_ORIENTATION].orientation.azimuth = ((float)sensorData.gyro.x)/256.0f;
        events[ID_ORIENTATION].orientation.pitch   = ((float)sensorData.gyro.y)/256.0f;
        events[ID_ORIENTATION].orientation.roll    = ((float)sensorData.gyro.z)/256.0f;
        events[ID_ORIENTATION].orientation.status  =
                SENSOR_STATUS_ACCURACY_HIGH;
        events[ID_ORIENTATION].type = SENSOR_TYPE_ORIENTATION;
    }

    ///if (sensorData.which & SENSOR_INT_COMPASS)
    {
        D("%s(fd=%d): read MAGNETIC_FIELD ", __FUNCTION__, fd);
        D("%s SENSOR_COMPASS_X : 0x%x \n", __FUNCTION__, sensorData.compass.x);
        D("%s SENSOR_COMPASS_Y : 0x%x \n", __FUNCTION__, sensorData.compass.y);
        D("%s SENSOR_COMPASS_Z : 0x%x \n", __FUNCTION__, sensorData.compass.z);

        new_sensors |= SENSORS_MAGNETIC_FIELD;
        events[ID_MAGNETIC_FIELD].magnetic.x = ((float)sensorData.compass.x)/256.0f;
        events[ID_MAGNETIC_FIELD].magnetic.y = ((float)sensorData.compass.y)/256.0f;
        events[ID_MAGNETIC_FIELD].magnetic.z = ((float)sensorData.compass.z)/256.0f;
        events[ID_MAGNETIC_FIELD].magnetic.status =
                SENSOR_STATUS_ACCURACY_HIGH;
        events[ID_MAGNETIC_FIELD].type = SENSOR_TYPE_MAGNETIC_FIELD;
    }

    if (new_sensors) {
        /** update the time of each new sensor event. */
        dev->pendingSensors |= new_sensors;
        int64_t t = (event_time < 0) ? 0 : event_time * 1000LL;

        /** use the time at the first sync: as the base for later
         * time values */
        if (dev->timeStart == 0) {
            dev->timeStart  = now_ns();
            dev->timeOffset = dev->timeStart - t;
        }
        t += dev->timeOffset;

        while (new_sensors) {
            uint32_t i = 31 - __builtin_clz(new_sensors);
            new_sensors &= ~(1U << i);
            dev->sensors[i].timestamp = t;
        }
    }
    return ret;
}

/**
 * @brief	Close sensor HAL.
 */
static int sensor_device_close(struct hw_device_t* dev0)
{
    SensorDevice* dev = (void*)dev0;
    /// Assume that there are no other threads blocked on poll()
    if (dev->fd >= 0) {
        close(dev->fd);
        dev->fd = -1;
    }
    pthread_mutex_destroy(&dev->lock);
    free(dev);
    return 0;
}

/**
 * @brief	Return an array of sensor data.
 *
 *          This function blocks until the sensor reports data.
 *          Note that according to the sensor HAL [1], it shall never return 0!
 *          [1] http://source.android.com/devices/sensors/hal-interface.html
 *
 * @param  	data	Returns an array of sensor data by filling the data argument
 * @retval 	the number of events read on success, or a negative error number in case of an error.
 */
static int sensor_device_poll(struct sensors_poll_device_t *dev0,
                              sensors_event_t* data, int count)
{
    SensorDevice* dev = (void*)dev0;
    D("%s: dev=%p data=%p count=%d ", __FUNCTION__, dev, data, count);

    if (count <= 0) {
        return -EINVAL;
    }

    int result = 0;
    pthread_mutex_lock(&dev->lock);

    /** If there is no data to be consumed, read data from sensor driver. */
    if (!dev->pendingSensors) {
        /** Block until there are pending events. Note that this releases
         * the lock during the blocking call, then re-acquires it before
         * returning. */
        int ret = sensor_device_poll_event_locked(dev);
        if (ret < 0) {
            E("%s: sensor_device_poll_event_locked return error %d ", __FUNCTION__, ret);
            result = ret;
            goto out;
        }
        /** This means there is no event even the read function returns. */
        if (!dev->pendingSensors) {
            E("%s: dev->pendingSensors is zero ", __FUNCTION__);
            result = -EIO;
            goto out;
        }
    }

    /** Now read as many pending events as needed. */
    int i;
    for (i = 0; i < count; i++)  {
        if (!dev->pendingSensors) {
            break;
        }

        /** Fill the requested event buffer with acquired data from the sensor driver.*/
        int ret = sensor_device_pick_pending_event_locked(dev, data);
        if (ret < 0) {
            if (!result) {
                E("%s: sensor_device_pick_pending_event_locked returns error %d ", __FUNCTION__, ret);
                result = ret;
            }
            break;
        }
        data++;
        result++;
    }

out:
    pthread_mutex_unlock(&dev->lock);
    D("%s: result=%d", __FUNCTION__, result);
    return result;
}

/**
 * @brief	Activates or deactivates a sensor.
 * @param  	handle	The handle of the sensor to activate/deactivate.
 * @param  	enabled set to 1 to enable or 0 to disable the sensor.
 * @retval 	returns 0 on success and a negative error number otherwise.
 */
static int sensor_device_activate(struct sensors_poll_device_t *dev0,
                                  int handle,
                                  int enabled)
{
    SensorDevice* dev = (void*)dev0;

    D("%s: handle=%s (%d) enabled=%d", __FUNCTION__,
        _sensorIdToName(handle), handle, enabled);

    /** Sanity check */
    if (!ID_CHECK(handle)) {
        E("%s: bad handle ID", __FUNCTION__);
        return -EINVAL;
    }

    /** Exit early if sensor is already enabled/disabled. */
    uint32_t mask = (1U << handle);
    uint32_t sensors = enabled ? mask : 0;

    pthread_mutex_lock(&dev->lock);

    uint32_t active = dev->active_sensors;
    uint32_t new_sensors = (active & ~mask) | (sensors & mask);
    uint32_t changed = active ^ new_sensors;

    int ret = 0;
    if (changed) {
        int fd = sensor_device_get_fd_locked(dev);
        if (fd < 0) {
            ret = fd;
            E("%s: failed to open sensor driver.", __FUNCTION__);
        }
        else {
            char enables = (char)new_sensors;
           	ret = write(fd, (void*)&enables, 1);
           	if(ret <= 0)
                E("%s: fail to activate handle=%s (%d) errno=%d: %s", __FUNCTION__,
                  _sensorIdToName(handle), handle, -ret, strerror(-ret));
            else
                dev->active_sensors = new_sensors;
        }
    }
    pthread_mutex_unlock(&dev->lock);
    return ret;
}

/**
 * @brief  After HAL version 1.0, this function is deprecated and is never called.
 *         In HAL version 1.0, setDelay was used instead of batch to set sampling_period_ns.
 */
static int sensor_device_set_delay(struct sensors_poll_device_t *dev0,
                                   int handle __unused,
                                   int64_t ns)
{
    SensorDevice* dev = (void*)dev0;

    int ms = (int)(ns / 1000000);
    D("%s: dev=%p delay-ms=%d", __FUNCTION__, dev, ms);

    char command[64];
    snprintf(command, sizeof command, "set-delay:%d", ms);

    pthread_mutex_lock(&dev->lock);
    int ret = sensor_device_send_command_locked(dev, command);
    pthread_mutex_unlock(&dev->lock);
    if (ret < 0) {
        E("%s: Could not send command: %s", __FUNCTION__, strerror(-ret));
    }
    return ret;
}

/**
 * @brief  MODULE REGISTRATION SUPPORT
 *         This is required so that hardware/libhardware/hardware.c
 *         will dlopen() this library appropriately.
 *
 * the following is the list of all supported sensors.
 * this table is used to build sSensorList declared below
 * according to which hardware sensors are reported as
 * available from the emulator (see get_sensors_list below)
 *
 * note: numerical values for maxRange/resolution/power for
 *       all sensors but light, pressure and humidity were
 *       taken from the reference AK8976A implementation
 *
 * @todo Let me check the resolution and the other values are proper
 * for the goldfish driver.
 */
static const struct sensor_t sSensorListInit[] = {
        { .name       = "Goldfish 3-axis Accelerometer",
          .vendor     = "The Android Open Source Project",
          .version    = 1,
          .handle     = ID_ACCELERATION,
          .type       = SENSOR_TYPE_ACCELEROMETER,
          .maxRange   = 2.8f,
          .resolution = 1.0f/4032.0f,
          .power      = 3.0f,
          .reserved   = {}
        },

        { .name       = "Goldfish 3-axis Magnetic field sensor",
          .vendor     = "The Android Open Source Project",
          .version    = 1,
          .handle     = ID_MAGNETIC_FIELD,
          .type       = SENSOR_TYPE_MAGNETIC_FIELD,
          .maxRange   = 2000.0f,
          .resolution = 1.0f,
          .power      = 6.7f,
          .reserved   = {}
        },

        { .name       = "Goldfish Orientation sensor",
          .vendor     = "The Android Open Source Project",
          .version    = 1,
          .handle     = ID_ORIENTATION,
          .type       = SENSOR_TYPE_ORIENTATION,
          .maxRange   = 360.0f,
          .resolution = 1.0f,
          .power      = 9.7f,
          .reserved   = {}
        },

        { .name       = "Goldfish Temperature sensor",
          .vendor     = "The Android Open Source Project",
          .version    = 1,
          .handle     = ID_TEMPERATURE,
          .type       = SENSOR_TYPE_TEMPERATURE,
          .maxRange   = 80.0f,
          .resolution = 1.0f,
          .power      = 0.0f,
          .reserved   = {}
        },

        { .name       = "Goldfish Proximity sensor",
          .vendor     = "The Android Open Source Project",
          .version    = 1,
          .handle     = ID_PROXIMITY,
          .type       = SENSOR_TYPE_PROXIMITY,
          .maxRange   = 1.0f,
          .resolution = 1.0f,
          .power      = 20.0f,
          .reserved   = {}
        },

        { .name       = "Goldfish Light sensor",
          .vendor     = "The Android Open Source Project",
          .version    = 1,
          .handle     = ID_LIGHT,
          .type       = SENSOR_TYPE_LIGHT,
          .maxRange   = 40000.0f,
          .resolution = 1.0f,
          .power      = 20.0f,
          .reserved   = {}
        },

        { .name       = "Goldfish Pressure sensor",
          .vendor     = "The Android Open Source Project",
          .version    = 1,
          .handle     = ID_PRESSURE,
          .type       = SENSOR_TYPE_PRESSURE,
          .maxRange   = 800.0f,
          .resolution = 1.0f,
          .power      = 20.0f,
          .reserved   = {}
        },

        { .name       = "Goldfish Humidity sensor",
          .vendor     = "The Android Open Source Project",
          .version    = 1,
          .handle     = ID_HUMIDITY,
          .type       = SENSOR_TYPE_RELATIVE_HUMIDITY,
          .maxRange   = 100.0f,
          .resolution = 1.0f,
          .power      = 20.0f,
          .reserved   = {}
        }
};

static struct sensor_t  sSensorList[MAX_NUM_SENSORS];

/**
 * @brief  Provides the list of sensors implemented by this HAL
 *         The order in which the sensors appear in the list is the order
 *	       in which the sensors will be reported to the applications.
 *
 * @retval 	The number of sensors in the list.
 */
static int sensors__get_sensors_list(struct sensors_module_t* module __unused,
        struct sensor_t const** list)
{
    char buffer[12];
    int  mask, nn, count;

    /** The goldfish driver supports the below sensors. So, mask(111b = 7) is set.
     *  #define  ID_ACCELERATION   (ID_BASE+0)
     *  #define  ID_MAGNETIC_FIELD (ID_BASE+1)
     *  #define  ID_ORIENTATION    (ID_BASE+2)
     */
    mask  = 7;
    count = 0;
    for (nn = 0; nn < MAX_NUM_SENSORS; nn++) {
        if (((1 << nn) & mask) == 0)
            continue;

        sSensorList[count++] = sSensorListInit[nn];
    }

    D("%s: returned %d sensors (mask=%d)", __FUNCTION__, count, mask);
    *list = sSensorList;

    return count;
}

/**
 * @brief	Open sensor HAL.
 */
static int
open_sensors(const struct hw_module_t* module,
             const char*               name,
             struct hw_device_t*      *device)
{
    int  status = -EINVAL;

    D("%s: name=%s", __FUNCTION__, name);

    if (!strcmp(name, SENSORS_HARDWARE_POLL)) {
        SensorDevice *dev = malloc(sizeof(*dev));

        memset(dev, 0, sizeof(*dev));

        dev->device.common.tag     = HARDWARE_DEVICE_TAG;
        dev->device.common.version = SENSORS_DEVICE_API_VERSION_1_0;
        dev->device.common.module  = (struct hw_module_t*) module;
        dev->device.common.close   = sensor_device_close;
        dev->device.poll           = sensor_device_poll;
        dev->device.activate       = sensor_device_activate;
        dev->device.setDelay       = sensor_device_set_delay;

        dev->fd = -1;
        pthread_mutex_init(&dev->lock, NULL);

        *device = &dev->device.common;
        status  = 0;
    }
    return status;
}


static struct hw_module_methods_t sensors_module_methods = {
    .open = open_sensors
};

struct sensors_module_t HAL_MODULE_INFO_SYM = {
    .common = {
        .tag = HARDWARE_MODULE_TAG,
        .version_major = 1,
        .version_minor = 0,
        .id = SENSORS_HARDWARE_MODULE_ID,
        .name = "Goldfish SENSORS Module",
        .author = "The Android Open Source Project",
        .methods = &sensors_module_methods,
    },
    .get_sensors_list = sensors__get_sensors_list
};